﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangePanel : MonoBehaviour
{
    public GameObject loginPanel;
    public GameObject registerPanel;

    public void ActivateLoginPanel()
    {
        if (loginPanel.activeInHierarchy)
        {
            return;
        }
        else
        {
            loginPanel.SetActive(!loginPanel.activeSelf);
            registerPanel.SetActive(!registerPanel.activeSelf);
        }
    }

    public void ActivateRegisterPanel()
    {
        if (registerPanel.activeInHierarchy)
        {
            return;
        }
        else
        {
            loginPanel.SetActive(!loginPanel.activeSelf);
            registerPanel.SetActive(!registerPanel.activeSelf);
        }
    }
}
