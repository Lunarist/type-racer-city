﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public RandomizeWord randomizeWord;
    string getChar;
    int i;
    

    void Start()
    {
        i = 0;
        randomizeWord.RandomWord();
    }

    void Update()
    {
        if (i < RandomizeWord.word.Length)
        {
            if (Input.anyKeyDown)
            {
                MatchingChar();
            }
        }
        else
        {
            randomizeWord.RandomWord();
            i = 0;
        }
    }

    void MatchingChar()
    {
        getChar = Input.inputString.ToUpper();
        char[] getCharArray = getChar.ToCharArray();
        char[] charArray = RandomizeWord.word.ToCharArray();

        if (getCharArray[0] == charArray[i])
        {
            i++;
            PhotonNetwork.player.AddScore(1);
            Debug.Log(PhotonNetwork.player.GetScore());
            Debug.Log("is Match");
            transform.Translate(transform.right * Time.deltaTime * 5.2f, Space.Self);
        }
        else
        {
            Debug.Log("is Wrong");
        }
    }
}
