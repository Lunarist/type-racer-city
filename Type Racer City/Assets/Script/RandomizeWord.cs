﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class RandomizeWord : MonoBehaviour
{
    int maxLetter = 150;
    public static string word;
    public List<string> word4letter;
    public List<string> word5letter;
    public List<string> word6letter;
    public Text wordText;

    public void RandomWord()
    {
        if (maxLetter > 0)
        {
            int randomList = Random.Range(0, 3);
            if (maxLetter > 110)
            { 
                word = word4letter[randomList];
                wordText.text = word;
                maxLetter -= 4;
            }
            else if (maxLetter > 60)
            {
                word = word5letter[randomList];
                wordText.text = word;
                maxLetter -= 5;
            }
            else if (maxLetter > 0)
            {
                word = word6letter[randomList];
                wordText.text = word;
                maxLetter -= 6;
            }
            else
            {
                Debug.Log("End");
            }
        }
    }
}
