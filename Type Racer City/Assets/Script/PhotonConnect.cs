﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class PhotonConnect : MonoBehaviour
{
    public static string gameVersion = "1.0";

    public void connectToPhoton()
    {
        Debug.Log("Connecting to photon ...");
        PhotonNetwork.ConnectUsingSettings(gameVersion);
        Debug.Log("Connected to master");
        PhotonNetwork.LoadLevel("MainMenu");
    }

    void OnFailedToConnectToPhoton(object parameters)
    {
        Debug.Log("OnFailedToConnectToPhoton. StatusCode: " + parameters + " ServerAddress: " + PhotonNetwork.ServerAddress);
        PhotonNetwork.ConnectUsingSettings(gameVersion);
    }

    void OnDisconnectedFromPhoton()
    {
        Debug.Log("Disconnected from photon");
        PhotonNetwork.ConnectUsingSettings(PhotonConnect.gameVersion);
    }
}
