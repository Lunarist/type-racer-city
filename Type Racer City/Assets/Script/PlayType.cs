﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayType : MonoBehaviour
{
    public RandomizeWord randomizeWord;
    public Text scoreText;

    bool saveChecker;
    string getChar;
    string key = "Save";
    int saveScore;
    int score;
    int i;

    void Start()
    {
        i = 0;
        score = 0;
        saveChecker = true;
        StartCoroutine(ScoreUpdate());
        scoreText.text = "Score : " + score.ToString();
    }

    void Update()
    {
        if (i < RandomizeWord.word.Length)
        {
            saveChecker = false;
            if (Input.anyKeyDown)
            {
                MatchingChar();
            }
        }
        else
        {
            saveChecker = true;
            randomizeWord.RandomWord();
            i = 0;
        }
       
        StartCoroutine(ScoreUpdate());
    }

    void MatchingChar()
    {
        getChar = Input.inputString.ToUpper();
        Debug.Log(getChar);

        char[] getCharArray = getChar.ToCharArray();
        char[] charArray = RandomizeWord.word.ToCharArray();

        if (getCharArray[0] == charArray[i])
        { 
            i++;
            score++;
            Debug.Log("is Match");
            scoreText.text = "Score : " + score.ToString();
        }
        else
        {
            Debug.Log("is Wrong");
        }

        if (score >= 150)
        {
            score = 150;
            //SaveTime();
            SceneManager.LoadSceneAsync("End");
        }
    }

    IEnumerator ScoreUpdate()
    {
        yield return new WaitUntil(() => saveChecker == true);
        SaveScore();
    }

    public void SaveScore()
    {
        PlayerPrefs.SetInt(key, score);
        saveScore = PlayerPrefs.GetInt(key); 
        Debug.Log("Score Saved " + saveScore);
    }

    /*public void SaveTime()
    {
        PlayerPrefs.SetFloat(key, Timer.timer);
        Timer.saveTime = PlayerPrefs.GetFloat(key);
        Debug.Log("Time Saved " + Timer.saveTime);
    }*/
}
