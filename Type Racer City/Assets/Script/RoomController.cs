﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomController : MonoBehaviour
{
    public GameObject playerPrefab1;
    public GameObject playerPrefab2;
    
    void Start()
    {
        if (!PhotonNetwork.connected)
        {
            PhotonNetwork.LoadLevel("MainMenu");
        }
        else
        {
            if (PhotonNetwork.isMasterClient)
            {
                PhotonNetwork.Instantiate(playerPrefab1.name, new Vector2(-7.16f, -2.92f), Quaternion.identity, 0);
            }
            else
            {
                PhotonNetwork.Instantiate(playerPrefab2.name, new Vector2(-5.5f, -0.85f), Quaternion.identity, 0);
            }
        }
    }

     void OnLeftRoom()
    {
        PhotonNetwork.LoadLevel("MainMenu");
    }

}
