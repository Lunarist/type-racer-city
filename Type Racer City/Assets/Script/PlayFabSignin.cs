﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using PlayFab;
using PlayFab.ClientModels;

public class PlayFabSignin : MonoBehaviour
{
    public InputField user;
    public InputField pass;
    public PhotonConnect pc;
    public Text status;
    string playFabPlayerID;
    
    public void SignIn()
    {   
        if (user.text.Contains("@"))
        {
            PlayFabClientAPI.LoginWithEmailAddress(new LoginWithEmailAddressRequest()
            {
                Email = user.text,
                Password = pass.text,
                /*InfoRequestParameters = new GetPlayerCombinedInfoRequestParams()
                {
                    GetPlayerProfile = true,
                    ProfileConstraints = new PlayerProfileViewConstraints()
                    {
                        ShowDisplayName = true
                    }
                }*/
            }, OnLoginSuccess, OnPlayFabError);
        }
        else
        {
            PlayFabClientAPI.LoginWithPlayFab(new LoginWithPlayFabRequest()
            {
                Username = user.text,
                Password = pass.text,
                /*InfoRequestParameters = new GetPlayerCombinedInfoRequestParams()
                {
                    GetPlayerProfile = true,
                    ProfileConstraints = new PlayerProfileViewConstraints()
                    {
                        ShowDisplayName = true
                    }
                }*/
            }, OnLoginSuccess, OnPlayFabError);
        }
    }

    void OnLoginSuccess(LoginResult result)
    {
        status.text = "Login Success";
        Debug.Log("PlayFab authenticated. Requesting photon token...");
        playFabPlayerID = result.PlayFabId;
        PlayFabClientAPI.GetPhotonAuthenticationToken(new GetPhotonAuthenticationTokenRequest()
        {
            PhotonApplicationId = PhotonNetwork.PhotonServerSettings.AppID
        }, AuthenticateWithPhoton, OnPlayFabError);
    }

    void AuthenticateWithPhoton(GetPhotonAuthenticationTokenResult obj)
    {
        Debug.Log("Photon token acquired: " + obj.PhotonCustomAuthenticationToken + "  Authentication complete.");
        AuthenticationValues customAuth = new AuthenticationValues { AuthType = CustomAuthenticationType.Custom };
        customAuth.AddAuthParameter("username", playFabPlayerID);  
        customAuth.AddAuthParameter("token", obj.PhotonCustomAuthenticationToken);
        PhotonNetwork.AuthValues = customAuth;
        pc.connectToPhoton();
    }

    void OnPlayFabError(PlayFabError obj)
    {
        Debug.Log(obj.GenerateErrorReport());
    }
}
