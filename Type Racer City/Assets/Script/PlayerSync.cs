﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSync : Photon.MonoBehaviour
{
    public MonoBehaviour[] localScripts;
    Vector3 latestPos;
    
    void Start()
    {
        if (photonView.isMine)
        {
            //Player is local
        }
        else
        {
            //Player is Remote
            for (int i = 0; i < localScripts.Length; i++)
            {
                localScripts[i].enabled = false;
            }
        }
    }

    void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting)
        {
            stream.SendNext(transform.position);
        }
        else
        {
            latestPos = (Vector3)stream.ReceiveNext();
        }
    }

    void Update()
    {
        if (!photonView.isMine)
        {
            transform.position = Vector3.Lerp(transform.position, latestPos, Time.deltaTime * 5.2f);
        }

        for (int j = 0; j < PhotonNetwork.playerList.Length; j++)
        {
            if (PhotonNetwork.playerList[j].GetScore() >= 150)
            {
                Debug.Log("Player " + j + " is the winner");
            }
        }
    }
}
