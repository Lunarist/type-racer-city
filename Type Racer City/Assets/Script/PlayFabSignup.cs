﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using PlayFab;
using PlayFab.ClientModels;

public class PlayFabSignup : MonoBehaviour
{
    public InputField user;
    public InputField pass;
    public InputField email;
    public ChangePanel panel;
    public Text status;
    public bool isRegistered = false;
    RegisterPlayFabUserRequest registerRequest;

    public void SignUp()
    {
        registerRequest = new RegisterPlayFabUserRequest();
        registerRequest.Username = user.text;
        registerRequest.Password = pass.text;
        registerRequest.Email = email.text;
        PlayFabClientAPI.RegisterPlayFabUser(registerRequest, result =>
        {
            status.text = "Register Success";
            panel.ActivateLoginPanel();
        }, error =>
        {
            Debug.Log(error.GenerateErrorReport());
        });
    }
}
